__help__ = """
 - /autochat: Type in reply to your message to activate. The bot will give auto replies to you\nCan also be used in PM's
 - /stopchat: Type in reply to your message to deactivate. The bot will stop giving auto replies to you
"""
__mod_name__ = "Chatterbot"
